import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
//
  import { SharedModule }from './shared/shared.module';
import { RegisModule } from './registro/registro.module';

@NgModule({
  declarations: [
    AppComponent,
    SharedModule,
    RegisModule
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
